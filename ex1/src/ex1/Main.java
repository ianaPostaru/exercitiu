package ex1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.*;


public class Main
{
	public long sumaCifre(long n1)
	{
		 long suma = 0;
		 while(n1!=0)
		 {
			 suma += n1%10;
			 n1 /= 10;
		 }
		 return suma;
	}
	
	public long factorial(long n2)
	{
		  if (n2 < 0) 
			  System.out.println("Eroare: n < 0");
		  else 
			  if (n2 > 20) 
				  System.out.println("Eroare: n prea mare"); // Daca n>20 depasim dimensiunea tipului long
		            else 
		            	if (n2 == 0) 
		                		return 1L;
		                   else 
		                	   return n2 * factorial (n2-1);
		                  return -1;
	}
	
	
	public static void main (String args[]) throws IOException
	{
		Main numar = new Main();
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		System.out.println("Dati numarul n = ");
		String s1 = br.readLine();
		long n = Integer.parseInt(s1);
	
		long i;
		long[] sum = new long[(int) (n+2)];
		long[] vector = new long[(int) (n+2)];
		for(i=0;i<=n;i++)
		{
			vector[(int) i] = numar.factorial(i);
		}
		
		
		for(i=0;i<=n;i++)
		{
			System.out.println (i+"! este = "+vector[(int) i]);
			sum[(int) i] = numar.sumaCifre(vector[(int) i]);
			System.out.println ("Suma cifrelor factorialului " + i + " este " + sum[(int) i]);
		}
		
		
		int bestIndex = 0;
		int bestLength = 0;
		int curIndex = 0;
		int curLength = 1;
		
		for(i=0;i<=n;i++)
		{
			if(sum[(int) i] <= sum[(int) (i+1)])
				curLength ++;
			else
			{
				curLength = 1;
			    curIndex = (int) i+1;
			}

			   if( curLength>bestLength)
			   {
			       bestIndex = curIndex;
			       bestLength = curLength;
			   }
				
		}
		
		System.out.println ("bestLength: "+bestLength);
		System.out.println ("bestIndex: " + bestIndex);
		
		
		System.out.print ("Secventa de numere care nu cresc: ");
		for(i=bestIndex; i<bestLength+bestIndex;i++)
			//System.out.print(sum[(int) i] + " ; ");//afiseaza direct suma cifrelor factorialului
			System.out.print(i+ " ; ");
			
		
		

		
	}
}